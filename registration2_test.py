from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

driver = Service(executable_path='chromedriver.exe')
browser = webdriver.Chrome(service=driver)

def test_second_check():
    browser.get('http://suninjuly.github.io/registration2.html')
    browser.find_element(By.CLASS_NAME, 'form-control.first').send_keys('Igor')
    browser.find_element(By.CLASS_NAME, 'form-control.third').send_keys('Bystrov')
    browser.find_element(By.CLASS_NAME, 'btn.btn-default').click()
    # self.assertEqual(browser.find_element_by_tag_name('h1').text,
    #                  'Congratulations! You have successfully registered!',
    #                  'Messages does not mach')
    #self.assertIn('Congratulations', browser.find_element(By.TAG_NAME, 'h1').text, 'Messages does not mach')
    assert 'Congratulations' in browser.find_element(By.TAG_NAME, 'h1').text, 'Messages does not mach'