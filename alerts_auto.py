import math
from selenium import webdriver
import time
import os

path = 'http://suninjuly.github.io/alert_accept.html'

try:
    browser = webdriver.Chrome(executable_path='chromedriver.exe')
    browser.get(path)

    browser.find_element_by_class_name('btn.btn-primary').click()

    alert = browser.switch_to.alert
    alert.accept()

    x = int(browser.find_element_by_id('input_value').text)

    def calc(x):
        return math.log(abs(12 * math.sin(x)))

    browser.find_element_by_id('answer').send_keys(calc(x))
    browser.find_element_by_class_name('btn.btn-primary').click()

    final_alert = browser.switch_to.alert.text
    temp = final_alert.split()
    print(temp[-1])


finally:
    time.sleep(5)
    browser.quit()

