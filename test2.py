import math
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

path = 'http://suninjuly.github.io/explicit_wait2.html'

try:
    browser = webdriver.Chrome(executable_path='chromedriver.exe')
    browser.get(path)

    button = browser.find_element(By.ID, 'book')
    WebDriverWait(browser, 12).until(EC.text_to_be_present_in_element((By.ID, 'price'), '100'))
    button.click()

    x = int(browser.find_element_by_id('input_value').text)

    def calc(x):
        return math.log(abs(12 * math.sin(x)))


    answer = browser.find_element_by_id('answer').send_keys(calc(x))
    submit = browser.find_element(By.ID, 'solve').click()

    # text = browser.find_element_by_id('verify_message').text
    # assert 'successful' in text


finally:
    time.sleep(5)
    browser.quit()