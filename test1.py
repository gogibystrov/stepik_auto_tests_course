import math
from selenium import webdriver
import time

path = 'http://suninjuly.github.io/redirect_accept.html'

try:
    browser = webdriver.Chrome(executable_path='chromedriver.exe')
    browser.get(path)

    browser.find_element_by_class_name('trollface.btn.btn-primary').click()

    first_window = browser.window_handles[0]
    second_window = browser.window_handles[1]

    browser.switch_to.window(second_window)
    time.sleep(1)

    x = int(browser.find_element_by_id('input_value').text)

    def calc(x):
        return math.log(abs(12 * math.sin(x)))


    browser.find_element_by_id('answer').send_keys(calc(x))
    browser.find_element_by_class_name('btn.btn-primary').click()

    final_alert = browser.switch_to.alert
    text_final_alert = final_alert.text
    temp = text_final_alert.split()
    print(temp[-1])


finally:
    time.sleep(5)
    browser.close()